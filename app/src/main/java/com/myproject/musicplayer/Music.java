package com.myproject.musicplayer;



import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Music {
    private String artist;
    private String musicName;
    private int artistImageRes;
    private int coverImageRec;
    private int musicFile;

    public int getMusicFile() {
        return musicFile;
    }

    public void setMusicFile(int musicFile) {
        this.musicFile = musicFile;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getMusicName() {
        return musicName;
    }

    public void setMusicName(String musicName) {
        this.musicName = musicName;
    }

    public int getArtistImageRes() {
        return artistImageRes;
    }

    public void setArtistImageRes(int artistImageRes) {
        this.artistImageRes = artistImageRes;
    }

    public int getCoverImageRec() {
        return coverImageRec;
    }

    public void setCoverImageRec(int coverImageRec) {
        this.coverImageRec = coverImageRec;
    }

    public static List<Music> getMusicList(){
        List<Music> musicList=new ArrayList<>();
        Music music1=new Music();
        music1.setArtist("Ehsan Khajeh Amiri");
        music1.setMusicFile(R.raw.music1);
        music1.setMusicName("Shahre Divooneh");
        music1.setArtistImageRes(R.drawable.artist1);
        music1.setCoverImageRec(R.drawable.cover1);

        Music music2=new Music();
        music2.setArtist("Mohsen Yeganeh");
        music2.setMusicFile(R.raw.music2);
        music2.setMusicName("Rage Khaab");
        music2.setArtistImageRes(R.drawable.artist2);
        music2.setCoverImageRec(R.drawable.cover2);

        Music music3=new Music();
        music3.setArtist("Reza Sadeghi");
        music3.setMusicFile(R.raw.music3);
        music3.setMusicName("Vaysa Donya");
        music3.setArtistImageRes(R.drawable.artist3);
        music3.setCoverImageRec(R.drawable.cover3);

        musicList.add(music1);
        musicList.add(music2);
        musicList.add(music3);

        return musicList;

    }
    public static String convertMillisToString(long durationInMillis) {
        long second = (durationInMillis / 1000) % 60;
        long minute = (durationInMillis / (1000 * 60)) % 60;

        return String.format(Locale.US, "%02d:%02d", minute, second);
    }
}

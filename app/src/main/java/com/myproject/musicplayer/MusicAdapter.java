package com.myproject.musicplayer;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;

import java.util.List;

public class MusicAdapter extends RecyclerView.Adapter<MusicAdapter.MusicViewHolder> {
    private List<Music> musics;
    private ItemEventListener ItemEventListener;
    private int playPo=-1;

    public MusicAdapter(List<Music> musics,ItemEventListener ItemEventListener){
        this.musics = musics;
        this.ItemEventListener = ItemEventListener;
    }

    @NonNull
    @Override
    public MusicViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MusicViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.music_item,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MusicViewHolder holder, int position) {
        holder.bindMusic(musics.get(position));
    }
    public void onNotifyMusic(Music music){
        int index=musics.indexOf(music);
        if(index != -1){
            if(index != playPo)
                notifyItemChanged(playPo);
            playPo=index;
            notifyItemChanged(playPo);
        }


    }

    @Override
    public int getItemCount() {
        return musics.size();
    }

    public class MusicViewHolder extends RecyclerView.ViewHolder{
        private TextView artistNameTv;
        private TextView musicNameTv;
        private ImageView artistIv;
        private LottieAnimationView animationView;
        public MusicViewHolder(@NonNull View itemView) {
            super(itemView);

            artistNameTv=itemView.findViewById(R.id.tv_artistNameItem);
            musicNameTv=itemView.findViewById(R.id.tv_musicNameItem);
            artistIv=itemView.findViewById(R.id.iv_artistItem);
            animationView=itemView.findViewById(R.id.animationView);

        }
        public void bindMusic(Music music){
            artistNameTv.setText(music.getArtist());
            musicNameTv.setText(music.getMusicName());
            artistIv.setImageResource(music.getCoverImageRec());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ItemEventListener.onMusicItemClicked(getAdapterPosition(),music);
                }
            });

            if(playPo==getAdapterPosition()){
                animationView.setVisibility(View.VISIBLE);
            }else
                animationView.setVisibility(View.GONE);
        }
    }
    public interface ItemEventListener {
        void onMusicItemClicked(int position ,Music music);
    }

}

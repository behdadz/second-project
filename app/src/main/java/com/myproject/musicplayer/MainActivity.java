package com.myproject.musicplayer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.android.material.slider.Slider;
import com.myproject.musicplayer.databinding.ActivityMainBinding;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements MusicAdapter.ItemEventListener {
    private ActivityMainBinding binding;
    private Timer timer;
    private MediaPlayer mediaPlayer;
    private RecyclerView recyclerView;
    private MusicAdapter musicAdapter;
    private List<Music> musics;
    private boolean isDrawing;
    private int result;
    private MusicState musicState=MusicState.STOP;

    @Override
    public void onMusicItemClicked(int position, Music music) {
        result=position;
        timer.cancel();
        timer.purge();
        mediaPlayer.release();
        getCurrentMusic(music);
    }

    enum MusicState{
        PLAY,STOP,PAUSE
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Fresco.initialize(this);
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        mediaPlayer=new MediaPlayer();
        musics=Music.getMusicList();

        recyclerView=findViewById(R.id.musicRv);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        musicAdapter=new MusicAdapter(musics,this);
        recyclerView.setAdapter(musicAdapter);

        binding.next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNext();
            }
        });
        binding.previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPrevious();
            }
        });

        binding.play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (musicState){
                    case PLAY:
                        mediaPlayer.pause();
                        musicState=MusicState.PAUSE;
                        binding.play.setImageResource(R.drawable.ic_play);
                    break;
                    case STOP:
                    case PAUSE:
                        mediaPlayer.start();
                        musicState=MusicState.PLAY;
                        binding.play.setImageResource(R.drawable.ic_pause);
                        break;
                }
            }
        });

        getCurrentMusic(musics.get(0));



    }

    public void getCurrentMusic(Music music){
        binding.musicSlider.setValue(0);
        musicAdapter.onNotifyMusic(music);
        mediaPlayer=MediaPlayer.create(MainActivity.this,music.getMusicFile());
    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayer mp) {
            mediaPlayer.start();
            timer=new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(!isDrawing)
                            binding.musicSlider.setValue(mediaPlayer.getCurrentPosition());
                        }
                    });


                }
            },1000,1000);
        }
    });

        binding.musicSlider.addOnChangeListener(new Slider.OnChangeListener() {
            @Override
            public void onValueChange(@NonNull Slider slider, float value, boolean fromUser) {
                binding.position.setText(Music.convertMillisToString((long) value));
            }
        });
        binding.musicSlider.addOnSliderTouchListener(new Slider.OnSliderTouchListener() {
            @Override
            public void onStartTrackingTouch(@NonNull Slider slider) {
                isDrawing=true;
            }

            @Override
            public void onStopTrackingTouch(@NonNull Slider slider) {
                mediaPlayer.seekTo((int) slider.getValue());
                isDrawing=false;
            }
        });
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
               onNext();
            }
        });
        binding.duration.setText(Music.convertMillisToString(mediaPlayer.getDuration()));
        binding.position.setText(Music.convertMillisToString(mediaPlayer.getCurrentPosition()));
        binding.musicSlider.setValueTo(mediaPlayer.getDuration());
        musicState=MusicState.PLAY;
        binding.play.setImageResource(R.drawable.ic_pause);

        binding.artistName.setText(music.getArtist());
        binding.musicName.setText(music.getMusicName());
        binding.cover.setActualImageResource(music.getCoverImageRec());
        binding.artistImage.setActualImageResource(music.getArtistImageRes());

    }

    public void onNext(){
        timer.purge();
        timer.cancel();
        mediaPlayer.release();
        if(result<musics.size()-1)
            result++;
        else
           result=0;
        getCurrentMusic(musics.get(result));
    }

    public void onPrevious(){
        timer.purge();
        timer.cancel();
        mediaPlayer.release();
        if(result==0)
            result=musics.size()-1;
        else
            result--;
        getCurrentMusic(musics.get(result));

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
        timer.purge();
        mediaPlayer.release();
        mediaPlayer=null;
    }
}